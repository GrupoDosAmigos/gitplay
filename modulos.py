def vipinfo():
    import xbmc
    import webbrowser
    xbmcgui.Dialog().ok('[COLOR deepskyblue][B]APOLLO[/COLOR][/B][COLOR gold][B] VIP[/COLOR][/B]', "[COLOR deepskyblue][B]O ACESSO AO APOLLO [COLOR gold][B]VIP[/COLOR][/B][COLOR deepskyblue][B] É POR[/COLOR][/B][COLOR orange][B] APLICATIVO[/COLOR][/B][COLOR deepskyblue][B] ENTRE EM CONTATO NOS LINKS A SEGUIR[/COLOR][/B]")
    dialog = xbmcgui.Dialog()
    link = dialog.select('[COLOR orange][B]ENTRE EM CONTADO NOS LINKS E BAIXA O APLICATIVO[/COLOR][/B]', ['[COLOR deepskyblue][B]OPÇÃO 1[/COLOR][/B][COLOR orange][B] TELEGRAM[/COLOR][/B]','[COLOR deepskyblue][B]OPÇÃO 2[/COLOR][/B][COLOR orange][B] WHATSAPP[/COLOR][/B]','[COLOR deepskyblue][B]BAIXAR APP PELO[/COLOR][/B][COLOR orange][B] APTOIDE[/COLOR][/B]','[COLOR deepskyblue][B]BAIXAR APP PELA[/COLOR][/B][COLOR orange][B] PLAY STORE[/COLOR][/B]','[COLOR deepskyblue][B]BAIXAR APP PELO[/COLOR][/B][COLOR orange][B] LINK DIRETO[/COLOR][/B][COLOR deepskyblue][B][/COLOR][/B][COLOR orange][B] HORIZON[/COLOR][/B][COLOR deepskyblue][B] P2P[/COLOR][/B]'])
    if link == 0:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://t.me/ApolloVIP' ) )
        else:
            webbrowser . open ( 'https://t.me/ApolloVIP' )
    if link == 1:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://api.whatsapp.com/send?phone=5588998165322&text=Acesso%20Vip' ) )
        else:
            webbrowser . open ( 'https://api.whatsapp.com/send?phone=5588998165322&text=Acesso%20Vip' )
    if link == 2:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://bit.ly/horizon-aptoide' ) )
        else:
            webbrowser . open ( 'http://bit.ly/horizon-aptoide' )
            
    if link == 3:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://bit.ly/horizonx-playstore' ) )
        else:
            webbrowser . open ( 'http://bit.ly/horizonx-playstore' )
            
            
    if link == 4:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://bit.ly/horizon-direto' ) )
        else:
            webbrowser . open ( 'http://bit.ly/horizon-direto' )



def suporte_addon():
    dialog = xbmcgui.Dialog()
    link = dialog.select('[COLOR orange][B]AJUDE O ADD-ON FAÇA SUA[/COLOR][/B][COLOR deepskyblue][B] DOAÇÃO[/COLOR][/B]', ['[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] SITE OFICIAL![/COLOR][/B]','[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] DOAÇÃO[/COLOR][/B][COLOR lightcyan][B] MERCADO[/COLOR][/B][COLOR dodgerblue][B]PAGO [/COLOR][/B][COLOR orange][B] R$10,00[/COLOR][/B]','[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] DOAÇÃO[/COLOR][/B][COLOR lightcyan][B] MERCADO[/COLOR][/B][COLOR dodgerblue][B]PAGO [/COLOR][/B][COLOR orange][B] R$15,00[/COLOR][/B]','[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] DOAÇÃO[/COLOR][/B][COLOR lightcyan][B] MERCADO[/COLOR][/B][COLOR dodgerblue][B]PAGO [/COLOR][/B][COLOR orange][B] R$20,00[/COLOR][/B]','[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] DOAÇÃO[/COLOR][/B][COLOR lightcyan][B] PIC[/COLOR][/B][COLOR lime][B]PAY [/COLOR][/B]','[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] GRUPO TELEGRAM[/COLOR][/B]','[COLOR deepskyblue][B]APOLLO ->[/COLOR][/B][COLOR orange][B] GRUPO FACEBOOK[/COLOR][/B]','[COLOR deepskyblue][B]ENTRAR NO[/COLOR][/B][COLOR orange][B] ADD-ON[/COLOR][/B]','[COLOR deepskyblue][B]SAIR DO[/COLOR][/B][COLOR orange][B] ADD-ON[/COLOR][/B]'])

    if link == 0:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://deus-apollo.ml' ) )
        else:
            webbrowser . open ( 'http://deus-apollo.ml' )
            
            
    if link == 1:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=203668628-f9a8d299-8b36-4cbd-84f4-0eb34ed0be11' ) )
        else:
            webbrowser . open ( 'https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=203668628-f9a8d299-8b36-4cbd-84f4-0eb34ed0be11' )
            
    if link == 2:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=203668628-98c1ff08-20de-477e-bde5-c779361cc791' ) )
        else:
            webbrowser . open ( 'https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=203668628-98c1ff08-20de-477e-bde5-c779361cc791' )
            
            
    if link == 3:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=203668628-bf5f9b8a-51d1-4ef3-9457-992bb522388c' ) )
        else:
            webbrowser . open ( 'https://www.mercadopago.com.br/checkout/v1/redirect?pref_id=203668628-bf5f9b8a-51d1-4ef3-9457-992bb522388c' )
            
            
    if link == 4:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://picpay.me/deus.apollo' ) )
        else:
            webbrowser . open ( 'https://picpay.me/deus.apollo' )
            
            

    if link == 5:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'http://telegram.me/DeusApollo' ) )
        else:
            webbrowser . open ( 'http://telegram.me/DeusApollo' )
    
    if link == 6:
        if xbmc . getCondVisibility ( 'system.platform.android' ) :
            xbmc . executebuiltin ( 'StartAndroidActivity(,android.intent.action.VIEW,,%s)' % ( 'https://www.facebook.com/groups/Deus.Apollo' ) )
        else:
            webbrowser . open ( 'https://www.facebook.com/groups/Deus.Apollo' )
    
    if link == 7:
     addon_log("SKindex")
     status_mensagem1 = addon.getSetting('mensagem1')
     if status_mensagem1 == 'False':
         xbmcgui.Dialog().ok(titulo_boas_vindas, descricao(url_mensagem_bem_vindo))
     addDir(titulo_vip,'',46,thumb_vip,FANART,descricao(url_desc_vip),'','','')
     ### BASE #############
     getData(url_principal,'')
     addDir(menu_atualizacao,'',45,thumb_update,FANART,descricao(url_desc_atualizacao),'','','')
     ### MENSAGEM #########
     mensagem()
     ######################
     xbmcplugin.endOfDirectory(int(sys.argv[1]))
    
#Apllo_CheckUpdate Inicial do Add-on
def Apllo_CheckUpdate(msg): #54
	try:
		uversao = urllib2.urlopen( "https://raw.githubusercontent.com/Tiagotj/REPOSITORIO-APOLLO/master/plugin.video.apollo/version.txt" ).read().replace('','').replace('\r','')
		uversao = re.compile('[a-zA-Z\.\d]+').findall(uversao)[0]
		#xbmcgui.Dialog().ok(Versao, uversao)
		if uversao != Versao:
			Update()
			#xbmc.executebuiltin("XBMC.Container.Refresh()")
		elif msg==True:
			#xbmcgui.Dialog().ok('[COLOR orange][B]APOLLO[/COLOR][/B]', "[COLOR deepskyblue][B] JÁ ESTÁ NA ÚLTIMA VERSÃO![/COLOR][/B] "+Versao+" [COLOR deepskyblue][B] ATUALIZAÇÕES SÃO AUTOMÁTICAS SE NÃO ATUALIZA ENTRE NO SITE[/COLOR][/B][COLOR orange][B] http://deus-apollo.ml[/COLOR][/B]\n[COLOR deepskyblue][B] USEM ESSE RECURSO CASO NÃO RECEBA AUTOMÁTICAMENTE[/COLOR][/B]")
			suporte_addon()
			#xbmc.executebuiltin("XBMC.Container.Refresh()")
	except urllib2.URLError, e:
		if msg==True:
			xbmcgui.Dialog().ok('Apollo', "Não foi possível checar")

def CheckUpdate(msg): #54
	try:
		uversao = urllib2.urlopen( "https://raw.githubusercontent.com/Tiagotj/REPOSITORIO-APOLLO/master/plugin.video.apollo/version.txt" ).read().replace('','').replace('\r','')
		uversao = re.compile('[a-zA-Z\.\d]+').findall(uversao)[0]
		#xbmcgui.Dialog().ok(Versao, uversao)
		if uversao != Versao:
			Update()
			#xbmc.executebuiltin("XBMC.Container.Refresh()")
		elif msg==True:
			xbmcgui.Dialog().ok('[COLOR orange][B]APOLLO[/COLOR][/B]', "[COLOR deepskyblue][B] O ADD-ON JÁ ESTÁ NA ÚLTIMA VERSÃO![/COLOR][/B][COLOR orange][B] 1.2.8[/COLOR][/B] "+Versao+"[COLOR deepskyblue][B] ATUALIZAÇÕES SÃO AUTOMÁTICAS SE NÃO ATUALIZA ENTRE NO SITE[/COLOR][/B][COLOR orange][B] http://deus-apollo.ml[/COLOR][/B]")
			#xbmc.executebuiltin("XBMC.Container.Refresh()")
	except urllib2.URLError, e:
		if msg==True:
			xbmcgui.Dialog().ok('Apollo', "Não foi possível checar")

def Update():
	Path = xbmc.translatePath( xbmcaddon.Addon().getAddonInfo('path') ).decode("utf-8")
	try:
		fonte = urllib2.urlopen( "https://raw.githubusercontent.com/Tiagotj/REPOSITORIO-APOLLO/master/plugin.video.apollo/apollo.py" ).read().replace('','')
		prog = re.compile('checkintegrity').findall(fonte)
		if prog:
			py = os.path.join( Path, "apollo.py")
			file = open(py, "w")
			file.write(fonte)
			file.close()
		fonte = urllib2.urlopen( "https://raw.githubusercontent.com/Tiagotj/REPOSITORIO-APOLLO/master/plugin.video.apollo/resources/settings.xml" ).read().replace('','')
		prog = re.compile('</settings>').findall(fonte)
		if prog:
			py = os.path.join( Path, "resources/settings.xml")
			file = open(py, "w")
			file.write(fonte)
			file.close()
		fonte = urllib2.urlopen( "https://raw.githubusercontent.com/Tiagotj/REPOSITORIO-APOLLO/master/plugin.video.apollo/addon.xml" ).read().replace('','')
		prog = re.compile('</addon>').findall(fonte)
		if prog:
			py = os.path.join( Path, "addon.xml")
			file = open(py, "w")
			file.write(fonte)
			file.close()
		xbmcgui.Dialog().ok('[COLOR deepskyblue][B]APOLLO[/COLOR][/B]', "OBS: ADD-ON ESTÁ DESATUALIZADO! CLIQUE EM OK PARA ELE SE ALTO ATUALIZAR! CASO O ADD-ON NÃO SE ATUALIZE FAÇA O DOWNLOAD NO SITE OFICIAL! [COLOR deepskyblue][B]->[/COLOR][/B][COLOR orange][COLOR orange][B] https://deus-apollo.ml[/COLOR][/B]").xbmc.executebuiltin
	except:
		xbmc.executebuiltin("Notification({0}, {1}, 9000, {2})".format(__addonname__, "[COLOR orange][B]Atualizando o addon. Por favor aguarde 5 segundos e Pode Continuar![/COLOR][/B]", __icon__))
        xbmc.executebuiltin("XBMC.Container.Refresh()")
        xbmcplugin.endOfDirectory(int(sys.argv[1]))
CheckUpdate(False)
        
def contador():
    request_headers = {    
    "Accept-Language": "en-US,en;q=0.5",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Referer": "APOLLO",
    "Connection": "keep-alive" 
    }
    request = urllib2.Request("https://whos.amung.us/pingjs/?k=c4302b", headers=request_headers)
    response = urllib2.urlopen(request).read()
    #tempo_delay = 1
    #xbmc.sleep(tempo_delay*0)
contador()


def lirodildos():
    import shutil
    for dildo in ['special://home/addons/plugin.video.tvfogohd','special://home/addons/repository.multirepo','special://home/addons/plugin.video.pikachu','special://home/addons/repo.pikachutop','special://home/addons/repo.pikachutop','special://home/addons/boiola1.repository','special://home/addons/boiola2.repository','special://home/addons/boiola3.repository','special://home/addons/Wizard.by.TONYWARLLEY','special://home/addons/frenchdj.repository','special://home/addons/griffin','special://home/addons/Kodish.repo.store','special://home/addons/kodish.repository','special://home/addons/novoigorrangel.repository','special://home/addons/plugin.program.aptoide','special://home/addons/plugin.program.BUILDSTWTUTORIAIS','special://home/addons/plugin.program.mbwiz','special://home/addons/plugin.program.tonywarlley','special://home/addons/plugin.video.9anime','special://home/addons/plugin.video.assistirme','special://home/addons/plugin.video.atto','special://home/addons/plugin.video.atto.filmes','special://home/addons/plugin.video.atto.series','special://home/addons/plugin.video.atto.torrents','special://home/addons/plugin.video.AttoFlix4K','special://home/addons/plugin.video.bb24horas','special://home/addons/plugin.video.cacaonova','special://home/addons/plugin.video.CaminhoFraterno','special://home/addons/plugin.video.canaldabiblia','special://home/addons/plugin.video.cangaco','special://home/addons/plugin.video.casteloratimbum','special://home/addons/plugin.video.cavalheiroszodiaco24hrs','special://home/addons/plugin.video.chaves24horas','special://home/addons/plugin.video.copadonordeste','special://home/addons/plugin.video.CubePlay','special://home/addons/plugin.video.dagay','special://home/addons/plugin.video.desenhos24hrs','special://home/addons/plugin.video.desenhostwt','special://home/addons/plugin.video.discovery','special://home/addons/plugin.video.docanimal','special://home/addons/plugin.video.DocUfo','special://home/addons/plugin.video.eupatroaeascriancas','special://home/addons/plugin.video.exodus','special://home/addons/plugin.video.fantasticc','special://home/addons/plugin.video.fdj.hd','special://home/addons/plugin.video.footballreplays','special://home/addons/plugin.video.gospeltv','special://home/addons/plugin.video.History world','special://home/addons/plugin.video.historychannel','special://home/addons/plugin.video.hotgoo','special://home/addons/plugin.video.hunterfm','special://home/addons/plugin.video.igorlista','special://home/addons/plugin.video.InovaTV','special://home/addons/plugin.video.iptvTWTUTORIAIS','special://home/addons/plugin.video.jfh','special://home/addons/plugin.video.jiraia','special://home/addons/plugin.video.kractosbr','special://home/addons/plugin.video.kratosteam','special://home/addons/plugin.video.largecamtube','special://home/addons/plugin.video.maluconopedaco','special://home/addons/plugin.video.mangoporn.net','special://home/addons/plugin.video.master','special://home/addons/plugin.video.masterchef','special://home/addons/plugin.video.miraculusof','special://home/addons/plugin.video.MMFilmesHD','special://home/addons/Plugin.video.MMmaroto','special://home/addons/plugin.video.mrbean','special://home/addons/plugin.video.nbaondemand','special://home/addons/plugin.video.ncs','special://home/addons/plugin.video.neptune','special://home/addons/plugin.video.nolecinema','special://home/addons/plugin.video.nostalgia','special://home/addons/plugin.video.odragonball24horas','special://home/addons/plugin.video.osmelhoresdomundo','special://home/addons/plugin.video.pandamovies.pw','special://home/addons/plugin.video.pegadinhasdosilviosantos','special://home/addons/plugin.video.picapau24horas','special://home/addons/plugin.video.placenta','special://home/addons/plugin.video.pornkino.to','special://home/addons/plugin.video.Raversunite','special://home/addons/plugin.video.RedeCanais(Lojink)','special://home/addons/plugin.video.REPO-LOJINK','special://home/addons/plugin.video.rodasemotores','special://home/addons/plugin.video.rsa','special://home/addons/plugin.video.sexkino.to','special://home/addons/plugin.video.sexuria.com','special://home/addons/plugin.video.simpsons24horas','special://home/addons/plugin.video.southpark24hrs','special://home/addons/plugin.video.streamingporn.xyz','special://home/addons/plugin.video.teamblue','special://home/addons/plugin.video.thewalkingdead','special://home/addons/plugin.video.todomundoodeiacris24horas','special://home/addons/plugin.video.TopCine','special://home/addons/plugin.video.Torrent Player','special://home/addons/plugin.video.TrapalhoesTV','special://home/addons/plugin.video.turmadamonica','special://home/addons/plugin.video.tvchannelhd','special://home/addons/plugin.video.Tv-Fogo-HD','special://home/addons/plugin.video.twtutoriais','special://home/addons/plugin.video.ufc-finest','special://home/addons/plugin.video.uwc','special://home/addons/plugin.video.venom','special://home/addons/plugin.video.venomreplays','special://home/addons/plugin.video.wildfire','special://home/addons/plugin.video.WorldOfWrestling','special://home/addons/plugin.video.XGames','special://home/addons/plugin.video.xtream-codes','special://home/addons/plugin.video.xvideos','special://home/addons/plugin.video.xxx.ghost','special://home/addons/plugin.video.xxxmenu','special://home/addons/plugin.video.xxx-o-dus','special://home/addons/plugin.video.you.jizz','special://home/addons/repo.TWTUTORIAIS','special://home/addons/repository.atto.team2.repository','special://home/addons/repository.blamo','special://home/addons/repository.exodusredux','special://home/addons/repository.maverickrepo','special://home/addons/repository.mrandmrssmith','special://home/addons/repository.StarTec','special://home/addons/repository.teamblue','special://home/addons/repository.thebishop','special://home/addons/script.areswizard','special://home/addons/script.exodus.artwork','special://home/addons/script.exodus.metadata','special://home/addons/script.Mod.by.TONYWARLLEY','special://home/addons/script.Mod.by.TONYWARLLEY.1.7','special://home/addons/script.Mod.by.TONYWARLLEY.1.8','special://home/addons/script.Mod.by.TONYWARLLEY.1.9','special://home/addons/script.Mod.by.TONYWARLLEY.2.0','special://home/addons/script.Mod.by.TONYWARLLEY.X.ONE','special://home/addons/script.module.exodus','special://home/addons/script.module.placenta','special://home/addons/script.module.urlresolver.xxx','special://home/addons/script.xxxodus.artwork','special://home/addons/script.xxxodus.scrapers','special://home/addons/service.subtitles.legendastv','special://home/addons/skin.red.edition17','special://home/addons/skin.special.dark.17','special://home/addons/skin.special.dark.k16','special://home/addons/skin.vipbuild17','special://home/addons/Wizard.by.TONYWARLLEY']:
        existe = xbmc.translatePath(dildo)
        if os.path.exists(existe)==True:
            shutil.rmtree(existe)
        else:
          pass
lirodildos()